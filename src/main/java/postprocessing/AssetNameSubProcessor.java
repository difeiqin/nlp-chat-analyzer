package postprocessing;

import entities.Entity;

public class AssetNameSubProcessor implements SubProcessTemplate {

    public AssetNameSubProcessor() {

    }

    @Override public Entity process(String entityName, String inputAssetName) {
        // todo: regex remove unncessary prefix/suffix/symbols
        // Pattern pStrip = Pattern.compile("blablabla");
        String cleaned = inputAssetName.trim();
        if (cleaned.startsWith("the")) {
            cleaned = cleaned.replaceFirst("the[ ]*", "");
        }

        return new Entity(entityName, cleaned, "String");
    }
}
