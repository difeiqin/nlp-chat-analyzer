package postprocessing;

import entities.Entity;
import org.ocpsoft.prettytime.nlp.PrettyTimeParser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DateTimeSubProcessor implements SubProcessTemplate {
    PrettyTimeParser timeParser;
    SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public DateTimeSubProcessor() {
        timeParser = new PrettyTimeParser();
    }

    @Override public Entity process(String entityName, String inputDateTimeString) {
        //todo: consider timezone?
        List<Date> results = timeParser.parse(inputDateTimeString);
        String resultStr = "";

        for (int i = 0; i < results.size(); i++) {
            if (Objects.nonNull(results.get(i))) {
                Date cur = results.get(i);
                resultStr = sm.format(cur);
                break;
            }
        }

        return new Entity(entityName, resultStr, "Datetime");
    }
}
