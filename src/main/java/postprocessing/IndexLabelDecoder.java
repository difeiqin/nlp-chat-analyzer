package postprocessing;

import javafx.util.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IndexLabelDecoder {
    public Map<Long, String> idLabelMap = new HashMap<Long, String>();
    public Map<String, Long> labelIdMap = new HashMap<String, Long>();
    public int intentSize = -1;

    public IndexLabelDecoder(String FilePath) {
        try {
            this.resetFromFile(FilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void resetFromFile(String FilePath) throws FileNotFoundException {
        Scanner sc = new Scanner(new FileInputStream(FilePath));
        idLabelMap.clear();
        labelIdMap.clear();

        long curIndex = 0L;
        while (sc.hasNextLine()) {
            String nextLabel = sc.nextLine().trim();
            if (nextLabel.length() > 0 && !nextLabel.equals("\n")) {
                idLabelMap.put(curIndex, nextLabel);
                labelIdMap.put(nextLabel, curIndex);
                curIndex++;
            }
        }
        intentSize = idLabelMap.size();
    }

    public List<String> decodeOneArray(long[] inputArray) {
        return Arrays.stream(inputArray).mapToObj(x -> idLabelMap.get(x)).collect(Collectors.toList());
    }

    public List<Long> encodeOneArray(String[] inputArray) {
        return Arrays.stream(inputArray).map(x -> labelIdMap.get(x)).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        // testing purpose
        IndexLabelDecoder intentDecoder = new IndexLabelDecoder("src/main/resources/ordered-intents.txt");
        System.out.println(intentDecoder.idLabelMap);
        System.out.println(intentDecoder.labelIdMap);

        IndexLabelDecoder slotDecoder = new IndexLabelDecoder("src/main/resources/ordered-slots.txt");
        System.out.println(slotDecoder.idLabelMap);
        System.out.println(slotDecoder.labelIdMap);

    }

    public int getIntentSize() {
        return intentSize;
    }

    public List<Pair<String, Double>> getDescIntentProbList(double[] probArray) {
        /***
         * Note this method by default round prob to 5 digits after decimal points.
         *
         */
        List<Integer> sortedIndices = IntStream.range(0, probArray.length).boxed().sorted(new Comparator<Integer>() {
            @Override public int compare(Integer o1, Integer o2) {
                if (probArray[o1] - probArray[o2] >= 0) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }).collect(Collectors.toList());

        List<Pair<String, Double>> sortedIntentProbList = new ArrayList<>();
        sortedIndices.forEach(i -> {
            sortedIntentProbList.add(new Pair<>(idLabelMap.get(new Long(i)), Math.round(probArray[i] * 100000)/100000d));
        });

        return sortedIntentProbList;
    }

}
