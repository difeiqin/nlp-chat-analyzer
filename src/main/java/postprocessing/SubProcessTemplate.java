package postprocessing;

import entities.Entity;

public interface SubProcessTemplate {
    public Entity process(String entityName, String inputAssetName);
}
