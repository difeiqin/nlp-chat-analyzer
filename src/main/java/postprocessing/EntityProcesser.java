package postprocessing;

import entities.Entity;
import entities.EntitySubProcessor;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class EntityProcesser {
    public static Map<String, EntitySubProcessor> entityNameProcessorMap;
    public AssetNameSubProcessor assetNameSubProcessor;
    public DateTimeSubProcessor dateTimeSubProcessor;

    static {
        // todo: externalize this?
        entityNameProcessorMap = new HashMap<>();
        entityNameProcessorMap.put("buy_asset_name", EntitySubProcessor.AssetNameProcessor);
        entityNameProcessorMap.put("sell_asset_name", EntitySubProcessor.AssetNameProcessor);
        entityNameProcessorMap.put("inquery_asset_name", EntitySubProcessor.AssetNameProcessor);

        entityNameProcessorMap.put("schedule_day", EntitySubProcessor.DateTimeProcessor);
        entityNameProcessorMap.put("schedule_time", EntitySubProcessor.DateTimeProcessor);
        entityNameProcessorMap.put("schedule_daytime", EntitySubProcessor.DateTimeProcessor);
    }

    public EntityProcesser() {
        assetNameSubProcessor = new AssetNameSubProcessor();
        dateTimeSubProcessor = new DateTimeSubProcessor();
    }

    public List<Entity> processEntities(List<String> tokens, List<String> slots) {
        List<IntermediateEntity> intermediates = combineSlotsForOneSent(tokens, slots);
        //        System.out.println("Tokens:" + tokens);
        //        System.out.println("Intermediates:");
        //        System.out.println(intermediates);

        List<IntermediateEntity> combinedEntities = combineRawEntities(intermediates);

        List<Entity> entities = standardizeEntity(combinedEntities);
        return entities;
    }

    public List<IntermediateEntity> combineRawEntities(List<IntermediateEntity> rawEntities) {
        /**
         For now, only combine data & time, if they are adjacent.

         This can only be used for "schedule_xxx" entity name, i.e. for demostration purpose only.
         */
        List<IntermediateEntity> results = new ArrayList<>();
        IntermediateEntity prev = null;

        for (int i = 0; i < rawEntities.size(); i++) {
            IntermediateEntity cur = rawEntities.get(i);
            // date time join
            if (!Objects.isNull(prev) && isAdjacent(prev, cur) && isDateTimeComplement(prev, cur)) {
                // DateTime combination
                // join prev with cur, pop last, push combined.
                String prefix = "schedule";
                String combinedName = prefix + "_daytime";
                // todo: two values need to be stripped via regex..
                String combinedValue = prev.getEntityValue() + " " + cur.getEntityValue();
                results.remove(results.size() - 1);
                results.add(new IntermediateEntity(combinedName, combinedValue, prev.getStartIndex(), cur.getEndIndex()));
            } else {
                results.add(rawEntities.get(i));
            }
            prev = results.get(results.size() - 1);
        }

        return results;
    }

    public boolean isAdjacent(IntermediateEntity prev, IntermediateEntity cur) {
        return prev.getEndIndex() == cur.getStartIndex();
    }

    public boolean isDateTimeComplement(IntermediateEntity prev, IntermediateEntity cur) {
        String[] prevNameParts = prev.getEntityName().toLowerCase().split("_");
        String[] curNameParts = cur.getEntityName().toLowerCase().split("_");

        String prevPrefix = String.join("_", Arrays.copyOfRange(prevNameParts, 0, prevNameParts.length - 1));
        String curPrefix = String.join("_", Arrays.copyOfRange(curNameParts, 0, curNameParts.length - 1));
        String prevLast = prevNameParts[prevNameParts.length - 1];
        String curLast = curNameParts[curNameParts.length - 1];

        boolean isComplement = prevLast.equals("time") && curLast.equals("day") || prevLast.equals("day") && curLast.equals("time");

        return prevPrefix.equals(curPrefix) && isComplement;
    }

    public List<IntermediateEntity> combineSlotsForOneSent(List<String> tokens, List<String> slots) {
        List<Pair<String, String>> result = new ArrayList<>();
        List<IntermediateEntity> resultList = new ArrayList<>();
        String prevEntity = "";
        String prevSlot = "";
        int startIndex = -1;

        for (int i = 0; i < slots.size(); i++) {
            String curSlot = slots.get(i);

            if (curSlot.equals("O")) {
                if (!prevEntity.equals("")) { // i.e there is entity to push
                    int endIndex = i;
                    result.add(new Pair<>(prevSlot.split("-", 2)[1], prevEntity));
                    resultList.add(new IntermediateEntity(prevSlot.split("-", 2)[1], prevEntity, startIndex, endIndex));
                    prevEntity = "";
                }
            }

            if (curSlot.startsWith("B")) {
                if (!prevEntity.equals("")) {
                    int endIndex = i;
                    result.add(new Pair<>(prevSlot.split("-", 2)[1], prevEntity));
                    resultList.add(new IntermediateEntity(prevSlot.split("-", 2)[1], prevEntity, startIndex, endIndex));
                }

                prevEntity = tokens.get(i);
                startIndex = i;
            }

            if (curSlot.startsWith("I")) {
                String curSlotStripped = curSlot.split("-", 2)[1];
                if (prevSlot.endsWith(curSlotStripped)) {
                    prevEntity = prevEntity + " " + tokens.get(i);
                } else {
                    if (!prevEntity.equals("")) {
                        int endIndex = i;
                        result.add(new Pair<>(prevSlot.split("-", 2)[1], prevEntity));
                        resultList.add(new IntermediateEntity(prevSlot.split("-", 2)[1], prevEntity, startIndex, endIndex));
                        prevEntity = "";
                    }
                }
            }
            prevSlot = curSlot;
        }

        if (!prevEntity.equals("")) {
            int endIndex = tokens.size();
            result.add(new Pair<>(prevSlot.split("-", 2)[1], prevEntity));
            resultList.add(new IntermediateEntity(prevSlot.split("-", 2)[1], prevEntity, startIndex, endIndex));
        }

        return resultList;
    }

    public List<Entity> standardizeEntity(List<IntermediateEntity> entities) {
        // based on slot name, standardize accordingly
        // buy/sell/inquiry asset name... text based method... maybe dictionary?
        // schedule day/time... convert to datetime object?

        // thus the return type should have original string. and also converted type obj,
        // to be defined in Entity class...?
        return entities.stream().map(e -> {
            String name = e.getEntityName();
            String value = e.getEntityValue();

            if (entityNameProcessorMap.get(name.toLowerCase()).equals(EntitySubProcessor.AssetNameProcessor)) {
                return assetNameSubProcessor.process(name, value);
            } else if (entityNameProcessorMap.get(name.toLowerCase()).equals(EntitySubProcessor.DateTimeProcessor)) {
                return dateTimeSubProcessor.process(name, value);
            } else
                return null;
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        //        List<String> slots = Arrays.asList();
        //        List<String> tokens = Arrays.asList();
        //
        //        EntityProcesser entityProcesser = new EntityProcesser();
        //        Entity temp = entityProcesser.assetNameProcessor("asset_name", "  the apple's stocks   ");
        //        entityProcesser.dateTimeProcessor("daytime", "next tuesday 2pm");
        //        System.out.println(temp);
    }

    public class IntermediateEntity {
        public String entityName;
        public String entityValue;
        public int startIndex; // inclusive
        public int endIndex; // exclusive

        public String getEntityName() {
            return entityName;
        }

        public String getEntityValue() {
            return entityValue;
        }

        public int getStartIndex() {
            return startIndex;
        }

        public int getEndIndex() {
            return endIndex;
        }

        public IntermediateEntity(String entityName, String entityValue, int startIndex, int endIndex) {
            this.entityName = entityName;
            this.entityValue = entityValue;
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }

        @Override public String toString() {
            return "IntermediateEntity{" + "entityName='" + entityName + '\'' + ", entityValue='" + entityValue + '\'' + ", startIndex=" + startIndex
                    + ", endIndex=" + endIndex + '}';
        }
    }
}