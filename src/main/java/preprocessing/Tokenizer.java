package preprocessing;

import util.Util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer {
    public static final Set<String> ENGLISH_LANGUAGE = new HashSet<String>(Arrays.asList("english", "en"));
    public static final Pattern ENGLISH_MATCHER = Pattern.compile("(.+?)[?.!]$");
    public static final String ENGLISH_DELIMITERS = "\\s+";

    public static final Set<String> FRENCH_LANGUAGE = new HashSet<String>(Arrays.asList("french", "fr"));

    public static List<List<String>> batchTokenize(List<String> inputs, String language) {
        if (ENGLISH_LANGUAGE.contains(language.toLowerCase())) {
            List<List<String>> allTokens = new ArrayList<>();
            for (String line : inputs) {
                allTokens.add(Arrays.asList(tokenize(line, "en")));
            }
            return allTokens;
        } else {
            return null;
        }
    }

    public static String[] tokenize(String input, String language) {
        if (ENGLISH_LANGUAGE.contains(language.toLowerCase())) {
            return defaultTokenize(input);
        } else {
            // for now, do nothing. return zero-length string.
            System.out.println("Error, input language is wrong...");
            return new String[0];
        }
    }

    private static String[] defaultTokenize(String input) {
        Matcher m = ENGLISH_MATCHER.matcher(input);
        String strippedLine = input;
        if (m.find()) {
            strippedLine = m.group(1);
        }
        //        System.out.println("Stripped line is :" + strippedLine);
        return strippedLine.split(ENGLISH_DELIMITERS);
    }
}
