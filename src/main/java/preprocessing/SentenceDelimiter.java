package preprocessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SentenceDelimiter {
    private static String splitPattern = "[.?!]+";

    public List<String> delimiteSentences(String sentences) {
        return Arrays.stream(sentences.split(splitPattern)).map(String::trim).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> testData = new ArrayList<>();
        testData.add("Hi Matteo, hope this message find you well. Could we meet sometime next week?");
        testData.add("Hi Thomas, hope everything is fine with you... Can you help me sell my stocks on Apple??!!! I need money now today!!!");

        SentenceDelimiter delimiter = new SentenceDelimiter();

        List<List<String>> results = testData.stream().map(l -> delimiter.delimiteSentences(l)).collect(Collectors.toList());

        results.forEach(l -> {
            System.out.println("Length of the result: " + l.size());
            l.forEach(System.out::println);
        });
    }
}
