package preprocessing;

import org.deeplearning4j.models.fasttext.FastText;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class TextEncoder {
    private static String ftModelPath = "src/main/resources/fasttext-model/cc.en.300.bin";
    private static FastText fastText = new FastText(new File(ftModelPath)); //binary model path
    public static int encodingDim = 300;

    public TextEncoder(String ftBinaryModelPath, int encodingDim) {
        try {
            fastText = new FastText(new File(ftBinaryModelPath));
            ftModelPath = ftBinaryModelPath;
            this.encodingDim = encodingDim;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TextEncoder() {

    }

    public int[] getLengths(List<List<String>> tokenLists) {
        return tokenLists.stream().mapToInt(l -> l.size()).toArray();
    }

    public int getMaxLength(int[] length) {
        return Arrays.stream(length).max().getAsInt();
    }

    public double[][][] encodeTokenLists(List<List<String>> tokenLists) {
        int[] lengths = getLengths(tokenLists);
        int maxLength = getMaxLength(lengths);

        double[][][] encodedInputs = new double[tokenLists.size()][maxLength][];

        for (int i = 0; i < encodedInputs.length; i++) {
            encodeOneTokenList(tokenLists.get(i), encodedInputs[i]);
        }

        return encodedInputs;
    }

    public void encodeOneTokenList(List<String> oneTokenList, double[][] targetAddress) {
        int i = 0;
        while (i < targetAddress.length) {
            if (i < oneTokenList.size()) {
                targetAddress[i] = fastText.getWordVector(oneTokenList.get(i));
            } else {
                targetAddress[i] = new double[encodingDim];
                Arrays.fill(targetAddress[i], 0.0);
            }
            i++;
        }
    }

    public int getEncodingDim() {
        return encodingDim;
    }

}
