import entities.Entity;
import entities.Party;
import entities.SentenceAnalysisResponse;
import javafx.util.Pair;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;
import postprocessing.EntityProcesser;
import postprocessing.IndexLabelDecoder;
import preprocessing.LanguageDetector;
import preprocessing.SentenceDelimiter;
import preprocessing.TextEncoder;
import preprocessing.Tokenizer;
import util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ChatAnalyzer {
    public static LanguageDetector languageDetector;
    public static SentenceDelimiter sentenceDelimiter;
    public static TextEncoder textEncoder;
    private static String advisorModelPath = "src/main/resources/advisor/advisor_pb300";
    private static String clientModelPath = "src/main/resources/client/client_pb300";
    private static SavedModelBundle advisorModel;
    private static SavedModelBundle clientModel;
    public static IndexLabelDecoder advisorIntentDecoder = new IndexLabelDecoder("src/main/resources/advisor/intents_vocab.txt");
    public static IndexLabelDecoder advisorSlotDecoder = new IndexLabelDecoder("src/main/resources/advisor/sf_labels_vocab.txt");
    public static IndexLabelDecoder clientIntentDecoder = new IndexLabelDecoder("src/main/resources/client/intents_vocab.txt");
    public static IndexLabelDecoder clientSlotDecoder = new IndexLabelDecoder("src/main/resources/client/sf_labels_vocab.txt");
    public EntityProcesser entityProcesser = new EntityProcesser();
    public Tokenizer tokenizer = new Tokenizer();

    public ChatAnalyzer() {
        languageDetector = new LanguageDetector();
        sentenceDelimiter = new SentenceDelimiter();
        textEncoder = new TextEncoder();
        advisorModel = SavedModelBundle.load(advisorModelPath, "serve");
        clientModel = SavedModelBundle.load(clientModelPath, "serve");
    }

    public List<SentenceAnalysisResponse> analyzeChat(String inputChat, Party party) {
        return analyzeChat(inputChat, party, 0.0);
    }

    public List<SentenceAnalysisResponse> analyzeChat(String inputChat, Party party, double minThreshold) {
        // for now, language is always english
        String targetLanguage = languageDetector.detectLanguage(inputChat);
        List<String> sentences = sentenceDelimiter.delimiteSentences(inputChat);
        List<List<String>> allTokens = tokenizer.batchTokenize(sentences, targetLanguage);
        double[][][] encodedInputs = textEncoder.encodeTokenLists(allTokens);

        int[] length = allTokens.stream().mapToInt(l -> l.size()).toArray();
        int maxLength = Arrays.stream(length).max().getAsInt();
        List<SentenceAnalysisResponse> responses = new ArrayList<>();

        // different model for different party
        if (party.equals(Party.ADVISOR)) {
            // Slot filling results
            Tensor<Long> sf_outs = advisorModel.session().runner().fetch("sf_outputs").feed("encoded_inputs", Tensor.<Double>create(encodedInputs))
                    .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0)
                    .expect(Long.class);

            long[][] sf_long = new long[allTokens.size()][maxLength]; //has to be primitive type and maxlength needs to be specified.
            sf_outs.copyTo(sf_long);
            List<List<String>> slots = Arrays.stream(sf_long).map(advisorSlotDecoder::decodeOneArray).collect(Collectors.toList());

            // Obtain probability for prediction
            Tensor<Double> intent_probs = advisorModel.session().runner().fetch("intent_probs").feed("encoded_inputs", Tensor.<Integer>create(encodedInputs))
                    .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0)
                    .expect(Double.class);

            double[][] probs = new double[encodedInputs.length][advisorIntentDecoder.getIntentSize()]; //has to be primitive type
            intent_probs.copyTo(probs);

            for (int i = 0; i < length.length; i++) {
                List<Pair<String, Double>> intentProbList = advisorIntentDecoder.getDescIntentProbList(probs[i]);
                List<Pair<String, Double>> filteredIntentProbList = intentProbList.stream().filter(x -> x.getValue() >= minThreshold)
                        .collect(Collectors.toList());
                //                List<Pair<String, String>> entities = entityProcesser.combineSlotsForOneSent(allTokens.get(i), slots.get(i).subList(0, length[i]));
                List<Entity> entities = entityProcesser.processEntities(allTokens.get(i), slots.get(i).subList(0, length[i]));
                responses.add(new SentenceAnalysisResponse(sentences.get(i), allTokens.get(i), filteredIntentProbList, entities));
            }

        } else if (party.equals(Party.CLIENT)) {
            // Slot filling results
            Tensor<Long> sf_outs = clientModel.session().runner().fetch("sf_outputs").feed("encoded_inputs", Tensor.<Double>create(encodedInputs))
                    .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0)
                    .expect(Long.class);

            long[][] sf_long = new long[allTokens.size()][maxLength]; //has to be primitive type and maxlength needs to be specified.
            sf_outs.copyTo(sf_long);

            List<List<String>> slots = Arrays.stream(sf_long).map(clientSlotDecoder::decodeOneArray).collect(Collectors.toList());

            // Obtain probability for prediction
            Tensor<Double> intent_probs = clientModel.session().runner().fetch("intent_probs").feed("encoded_inputs", Tensor.<Integer>create(encodedInputs))
                    .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0)
                    .expect(Double.class);

            double[][] probs = new double[encodedInputs.length][clientIntentDecoder.getIntentSize()]; //has to be primitive type
            intent_probs.copyTo(probs);

            for (int i = 0; i < length.length; i++) {
                List<Pair<String, Double>> intentProbList = clientIntentDecoder.getDescIntentProbList(probs[i]);
                List<Pair<String, Double>> filteredIntentProbList = intentProbList.stream().filter(x -> x.getValue() >= minThreshold)
                        .collect(Collectors.toList());
                //                List<Pair<String, String>> entities = entityProcesser.combineSlotsForOneSent(allTokens.get(i), slots.get(i).subList(0, length[i]));
                List<Entity> entities = entityProcesser.processEntities(allTokens.get(i), slots.get(i).subList(0, length[i]));
                responses.add(new SentenceAnalysisResponse(sentences.get(i), allTokens.get(i), filteredIntentProbList, entities));
            }
        }

        return responses;
    }

    public static void main(String[] args) {
        String testChat = "Hi, hope this message find you well. Would it be convenient to meet some time next Wednesday 5pm? Let's meet next Tuesday .";
        ChatAnalyzer testChatAnalyzer = new ChatAnalyzer();
        List<SentenceAnalysisResponse> responses = testChatAnalyzer.analyzeChat(testChat, Party.ADVISOR);
        responses.forEach(System.out::println);

        System.out.println("\n============================================\n");

        String testChat2 = "Hi, hope this message find you well. Can you help me sell IBM today? Let's meet the day after tomorrow .";
        List<SentenceAnalysisResponse> responses2 = testChatAnalyzer.analyzeChat(testChat2, Party.CLIENT);
        responses2.forEach(System.out::println);
    }
}
