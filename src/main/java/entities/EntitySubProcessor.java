package entities;

public enum EntitySubProcessor {
    AssetNameProcessor, DateTimeProcessor;
}
