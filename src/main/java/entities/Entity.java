package entities;

public class Entity {
    public String name;
    public String value;
    public String dataType;

    public Entity(String name, String value, String dataType) {
        this.name = name;
        this.value = value;
        this.dataType = dataType;
    }

    @Override public String toString() {
        return "Entity{" + "name='" + name + '\'' + ", value='" + value + '\'' + ", dataType='" + dataType + '\'' + '}';
    }
}
