package entities;

import javafx.util.Pair;

import java.util.List;

public class SentenceAnalysisResponse {
    String originalSentence;
    List<String> tokens;
    List<Pair<String, Double>> intentProbs;
    List<Entity> params;

    public SentenceAnalysisResponse(String originalSentence, List<String> tokens, List<Pair<String, Double>> intentProbs, List<Entity> entities) {
        this.originalSentence = originalSentence;
        this.tokens = tokens;
        this.intentProbs = intentProbs;
        this.params = entities;
    }

    @Override public String toString() {
        return "SentenceAnalysisResponse{" + "originalSentence='" + originalSentence + '\'' + ",\n tokens=" + tokens + ",\n intentProbs=" + intentProbs
                + ",\n params=" + params + '}';
    }
}