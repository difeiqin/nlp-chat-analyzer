package experiments;

import org.tensorflow.*;

public class TutorialExperiment {
    public static void main(String[] args) {
//        System.out.println("Main class in Joint model");
//        z = 3 * x + 2 * y
        Graph graph = new Graph();

        //define constants
        // todo: what are the all available op types?
        // todo: note how Tensor is being built...
        Operation a = graph.opBuilder("Const", "a").setAttr("dtype", DataType.fromClass(Double.class))
                .setAttr("value", Tensor.<Double>create(3.0, Double.class)).build();

        Operation b = graph.opBuilder("Const", "b").setAttr("dtype", DataType.DOUBLE)
                .setAttr("value", Tensor.<Double>create(2.0, Double.class)).build();

        // define placeholder
        Operation x = graph.opBuilder("Placeholder", "x").setAttr("dtype", DataType.DOUBLE)
                .build();
        Operation y = graph.opBuilder("Placeholder", "y").setAttr("dtype", DataType.DOUBLE)
                .build();

        // defining functions
        // todo: note here the dimension index specified in output function, check API page
        // todo; why output needed? because it is operation??
        Operation ax = graph.opBuilder("Mul", "ax").addInput(a.output(0)).addInput(x.output(0))
                .build();
        Operation by = graph.opBuilder("Mul", "by").addInput(b.output(0)).addInput(y.output(0))
                .build();

        Operation z = graph.opBuilder("Add", "z").addInput(ax.output(0)).addInput(by.output(0))
                .build();

        // NOt what we want, because placeholders haven't got their values yet...
        System.out.println("result of computation is " + z.output(0));

        Session sess = new Session(graph);
        // todo: how to pass(fecth & feed) values in for placeholder?
        //  check documentation for API details...
        Tensor<Double> tensor = sess.runner().fetch("z").feed("x", Tensor.<Double>create(3.0, Double.class))
                .feed("y", Tensor.<Double>create(6.0, Double.class))
                .run().get(0).expect(Double.class); //todo: why get and expect are needed here?

        System.out.println("Output after feeding is... " + tensor.doubleValue());

        // todo: save and load? Google has this thing called Protocle Buffer(pb),
        //  which allows generating model files in language-neutral files, see https://www.baeldung.com/tensorflow-java
        //  Thus, you need to save python model in pd format and load it in java...
        // todo: also see https://zhuanlan.zhihu.com/p/32887066
    }
}
