package experiments;

import javafx.util.Pair;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;
import postprocessing.IndexLabelDecoder;
import preprocessing.TextEncoder;
import preprocessing.Tokenizer;

import java.util.*;
import java.util.stream.Collectors;

public class OOVExperiment {
    private static String tfModelPath = "/Users/dqin/Documents/workspace/tensorflow-experiment/src/main/resources/tfmodel/pb300_l2_data5_11_intents";
    private static SavedModelBundle tfModel = SavedModelBundle.load(tfModelPath, "serve");
    public static Tokenizer tokenizer = new Tokenizer();

    public static void main(String[] args) {
        TextEncoder textEncoder = new TextEncoder();
        System.out.println("Encoding dimension is: " + textEncoder.getEncodingDim());

        List<String> inputs = new ArrayList<String>();
        inputs.add("I'm interested in Apple's stocks");
        //        inputs.add("I hope you can sell all my holdings on IBM now");
        //        inputs.add("I want to sell all my apple stocks");

        List<List<String>> allTokens = tokenizer.batchTokenize(inputs, "en");
        int[] length = allTokens.stream().mapToInt(l -> l.size()).toArray();
        int maxLength = Arrays.stream(length).max().getAsInt();

        double[][][] encodedInputs = textEncoder.encodeTokenLists(allTokens);

        //        todo: try Fix this by loading a vector file? rather than binary file?
        //        todo: after loading vector files, try getwordvectors for OOV word and see how...

        // Slot filling results
        Tensor<Long> sf_outs = tfModel.session().runner().fetch("sf_outputs").feed("encoded_inputs", Tensor.<Double>create(encodedInputs))
                .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0).expect(Long.class);

        long[][] sf_long = new long[allTokens.size()][maxLength];
        //note, it has to be primitive type and maxlength needs to be specified.
        sf_outs.copyTo(sf_long);

        System.out.println("Slot Filling Result: ");
        for (int i = 0; i < sf_long.length; i++) {
            System.out.println(Arrays.toString(Arrays.copyOfRange(sf_long[i], 0, length[i])));
        }

        // Intent detection
        Tensor<Long> intent_outs = tfModel.session().runner().fetch("intent_outputs").feed("encoded_inputs", Tensor.<Integer>create(encodedInputs))
                .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0).expect(Long.class);
        long[] intent_int = new long[encodedInputs.length]; //note, it has to be primitive type
        intent_outs.copyTo(intent_int);
        System.out.println("Intent Result: " + Arrays.toString(intent_int));

        IndexLabelDecoder intentDecoder = new IndexLabelDecoder("src/main/resources/ordered-intents.txt");
        IndexLabelDecoder slotDecoder = new IndexLabelDecoder("src/main/resources/ordered-slots.txt");

        List<String> intents = intentDecoder.decodeOneArray(intent_int);
        List<List<String>> slots = Arrays.stream(sf_long).map(slotDecoder::decodeOneArray).collect(Collectors.toList());

        // Obtain probability for prediction
        Tensor<Double> intent_probs = tfModel.session().runner().fetch("intent_probs").feed("encoded_inputs", Tensor.<Integer>create(encodedInputs))
                .feed("batch_input_length", Tensor.<Integer>create(length)).feed("keep_prob", Tensor.create(1.0, Double.class)).run().get(0)
                .expect(Double.class);
        double[][] probs = new double[encodedInputs.length][intentDecoder.getIntentSize()]; //note, it has to be primitive type
        intent_probs.copyTo(probs);

        // print results
        System.out.println("All Tokens:");
        allTokens.forEach(System.out::println);

        System.out.println("\n" + "Translated Results:");
        System.out.println(intents);
        for (int i = 0; i < length.length; i++) {
            System.out.println(slots.get(i).subList(0, length[i]));
            List<Pair<String, Double>> intentProbList = intentDecoder.getDescIntentProbList(probs[i]);
            System.out.println("Sorted prob list...");
            System.out.println(intentProbList);
        }

    }

    public static void printIntentProb(Map<Long, String> idIntentMap, double[] probArray) {
        for (long i = 0; i < probArray.length; i++) {
            System.out.println(idIntentMap.get(i) + " " + probArray[(int) i]);
        }

        SortedMap<String, Double> temp = new TreeMap<>(new Comparator<String>() {
            @Override public int compare(String o1, String o2) {
                return 0;
            }
        });
        //sorted hash map?
    }
}
