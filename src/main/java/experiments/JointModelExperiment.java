package experiments;

import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;

import java.util.Arrays;

public class JointModelExperiment {
    public static void main(String[] args) {
        SavedModelBundle model = SavedModelBundle.load("/Users/dqin/Documents/workspace/" +
                "tensorflow-experiment/src/main/resources/tfmodel/pb", "serve");

        // how to convert java doubles arrays/list into tensorflow datatype?
        // todo:  check documentation....
        int[][] input = {{
//                6, 36, 41, 119, 38, 3, 7, 44, 19, 200, 159, 50, 73
                14, 16, 44, 19, 175, 33, 24, 62, 101
        }};

        int[] length = {9};

        // Slot Filling call:
        Tensor<Long> sf_outs = model.session().runner().fetch("sf_outputs")
                .feed("batch_input", Tensor.<Integer>create(input))
                .feed("batch_input_length", Tensor.<Integer>create(length))
                .feed("keep_prob", Tensor.create(1.0f, Float.class))
                .run().get(0).expect(Long.class);

        long[][] sf_int = new long[1][9]; //note, it has to be primitive type
        sf_outs.copyTo(sf_int);
        System.out.println("Slot Filling Result: " + Arrays.deepToString(sf_int));
        // https://stackoverflow.com/questions/409784/whats-the-simplest-way-to-print-a-java-array


        // Intent Detection call:
        Tensor<Long> intent_outs = model.session().runner().fetch("intent_outputs")
                .feed("batch_input", Tensor.<Integer>create(input))
                .feed("batch_input_length", Tensor.<Integer>create(length))
                .feed("keep_prob", Tensor.create(1.0f, Float.class))
                .run().get(0).expect(Long.class);
        long[] intent_int = new long[1]; //note, it has to be primitive type
        intent_outs.copyTo(intent_int);
        System.out.println("Intent Result: " + Arrays.toString(intent_int));

//        String[] results = util.Util.tokenize("what a love day it is in Singapore !", "en");
//        System.out.println("Tokeniation result " + Arrays.toString(results));

        // also read: https://medium.com/google-cloud/how-to-invoke-a-trained-tensorflow-model-from-java-programs-27ed5f4f502d
    }
}