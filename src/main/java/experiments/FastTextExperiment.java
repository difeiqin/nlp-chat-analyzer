package experiments;

import org.deeplearning4j.models.fasttext.*;
import org.nd4j.linalg.api.ndarray.INDArray;

import java.io.File;
import java.util.Arrays;

public class FastTextExperiment {
    private static String modelPathName = "/Users/dqin/Downloads/fasttext/cc.en.100.bin";

    public static void main(String[] args) {
        File modelFile = new File(modelPathName);
        FastText fastText = new FastText(modelFile);//binary path model

        double[] wordVector = fastText.getWordVector("apple");
        INDArray wordMatrix = fastText.getWordVectorMatrix("appleeeee");

        System.out.println(Arrays.toString(wordVector));
        System.out.println("\n");
//        System.out.println(wordMatrix.toString());

        System.out.println(wordMatrix.toStringFull());
        double[] wordVector2 = fastText.getWordVector("ibm");
        System.out.println(Arrays.toString(wordVector2));

    }

}
