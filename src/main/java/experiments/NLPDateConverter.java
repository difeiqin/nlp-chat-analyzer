package experiments;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.ocpsoft.prettytime.nlp.PrettyTimeParser;
//import com.joestelmach.natty.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class NLPDateConverter {
    //refer to:  https://stackoverflow.com/questions/1410408/natural-language-date-and-time-parser-for-java
    // two options: prettytime:nlp or natty
    public static List<String> data = new ArrayList<>();

    static {
        data.add("this Wednesday evening 3PM");
        data.add("next wednesday 3pm");
        data.add("3pm this wednesday");
        data.add("3pm next wednesday");

        data.add("next Friday 11 am");
        data.add("next next Friday 11am");
        data.add("the friday after next, 11 am");
        data.add("11 am, the friday after next");

        data.add("this Sunday 10 pm");
        data.add("next Sunday 10pm");
        data.add("the coming Sunday 10pm");

        data.add("this monday 1 am");
        data.add("next monday 1 am");
        data.add("the coming monday 1 am");

        data.add("the day before next thursday");
    }

    public static void main(String[] args) {
        System.out.println("Pretty Time results:");
        prettyTimeTest();
//        System.out.println("\nNatty results:");
//        nattyTest();

    }

//    public static void nattyTest() {
//        //        http://natty.joestelmach.com/doc.jsp
//        Parser parser = new Parser();
//        data.forEach(d -> {
//            List<DateGroup> groups = parser.parse(d);
//            System.out.println("Length of group: " + groups.size());
//            System.out.println(d + " " + groups.get(0).getDates());
//        });
//
//        //        List<DateGroup> groups = parser.parse("the day before next thursday");
//        //        for (DateGroup group : groups) {
//        //            List dates = group.getDates();
//        //            int line = group.getLine();
//        //            int column = group.getPosition();
//        //            String matchingValue = group.getText();
//        //            String syntaxTree = group.getSyntaxTree().toStringTree();
//        //            Map<String, List<ParseLocation>> parseMap = group.getParseLocations();
//        //            boolean isRecurreing = group.isRecurring();
//        //            Date recursUntil = group.getRecursUntil();
//        //            System.out.println("Gourp:" + group.getDates());
//        //            System.out.println(dates);
//        //            System.out.println(line);
//        //            System.out.println(column);
//        //            System.out.println(matchingValue);
//        //            System.out.println(syntaxTree);
//        //            System.out.println(isRecurreing);
//    }

    public static void prettyTimeTest() {
        // todo: go figure out more details about this library... e.g. how to set the current(reference) date?

        //        List<Date> dates = new PrettyTimeParser().parse("I'm going to the beach in three days!");
        PrettyTimeParser prettyTimeParser = new PrettyTimeParser();

        List<List<Date>> results = new ArrayList<>();

        data.forEach(d -> {
            results.add(prettyTimeParser.parse(d));
        });

        for (int i = 0; i < results.size(); i++) {
            System.out.println(data.get(i) + "\t" + results.get(i));
        }
    }
}
