import entities.Party;
import entities.SentenceAnalysisResponse;

import java.util.List;

public class Tester {
    public static ChatAnalyzer chatAnalyzer = new ChatAnalyzer();

    public static void main(String[] args) {
        String chat1 = "Hi, hope this message find you well." + " Would it be convenient to meet face to face?" + " Let's meet next Tuesday 5pm.";

        String chat2 = "Hi, hope this message find you well." + " Can you help me sell IBM today?" + " Let's meet the day after tomorrow 11am.";

        String chat3 = "Can we have a call at 3pm?" + "Can we meet next Monday?" + "Can we meet next Thursday 11am?";

        List<SentenceAnalysisResponse> r1 = chatAnalyzer.analyzeChat(chat1, Party.ADVISOR, 0.05);
        r1.forEach(r -> {
            System.out.println(r + "\n");
        });
        System.out.println("\n================\n");

        List<SentenceAnalysisResponse> r2 = chatAnalyzer.analyzeChat(chat2, Party.CLIENT, 0.05);
        r2.forEach(r -> {
            System.out.println(r + "\n");
        });
        System.out.println("\n================\n");

        List<SentenceAnalysisResponse> r3 = chatAnalyzer.analyzeChat(chat3, Party.CLIENT, 0.05);
        r3.forEach(r -> {
            System.out.println(r + "\n");
        });

    }
}
